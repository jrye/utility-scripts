#!/usr/bin/env bash
set -euo pipefail

if [[ $# -lt 1 ]]
then
  cat >&2 << USAGE
usage: ${0##*/} [rsync options] <destination>

Copy the files of the current working directory's top-level Git directory to the
given destination using rsync. This is practical for deploying a Git repository
and its submodules via SSH to a server on which it is not possible or practical
to clone the Git files directly.

All files ignored by Git will be omitted from the copy. All current
modifications in the working directory will be copied.
USAGE
  exit 1
fi

toplevel=$(git rev-parse --show-toplevel)


args=("$@")
dest=${args[-1]}
unset args[-1]

cmd=(rsync --filter=':- .gitignore' -rtL "${args[@]}" "${toplevel}/" "$dest")
echo "${cmd[*]@Q}"
"${cmd[@]}"
