#!/usr/bin/env bash
set -euo pipefail

function show_help()
{
  cat <<HELP
SYNOPSIS

  Remove ignored files.

USAGE

  ${0##*/} [-h] [-c]

OPTIONS

  -h
    Show this message and exit.

  -c
    Confirm removal. Without this option. a dry run is performed.

HELP
  exit "$1"
}

confirm=false
while getopts 'c' flag
do
  case "$flag" in
    c) confirm=true ;;
    h) show_help 0 ;;
    *) show_help 1 ;;
  esac
done

toplevel=$(git rev-parse --show-toplevel)

cmd=(git -C "$toplevel" clean -fdX)
if ! $confirm
then
  cmd+=(-n)
fi
echo "${cmd[*]@Q}"
"${cmd[@]}"
