#!/usr/bin/env bash
set -euo pipefail

function show_help()
{
  cat << HELP
SYNOPSIS

  Install Python packages with Pip. This handles a bug in hatch-vcs /
  setuptools-scm which would otherwise prevent installation of packages from Git
  submodules.

USAGE

  ${0##*/} [-h] <path> [<path> ...]

  Install local Python packages at the given paths with pip.

OPTIONS

  -h
    Show this help message.

HELP
  exit "$1"
}

while getopts 'h' flag
do
  case $flag in
    h)
      show_help 0
      ;;
    *)
      show_help 1
      ;;
  esac
done
shift $((OPTIND - 1))

pip_install=(pip install --upgrade)
for pkg_dir in "$@"
do
  if [[ \
    -e $pkg_dir/requirements.txt && \
    ! -e $pkg_dir/pyproject.toml && \
    ! -e $pkg_dir/setup.py \
  ]]
  then
    "${pip_install[@]}" -r "$pkg_dir/requirements.txt"

  # The .git path will be a directory in a regular repository but a file in a
  # submodule. The file will contain a field named "gitdir" with a relative path
  # to submodule's git directory in the parent module.
  elif [[ -d $pkg_dir/.git ]]
  then
    "${pip_install[@]}" "${pkg_dir}"
  else
    pushd "$pkg_dir" >/dev/null
    # Get the absolute path to the git directory.
    gitdir=$(sed -n '/^gitdir: /s/gitdir: //p' ./.git)
    gitdir=$(readlink -f "$gitdir")
    popd >/dev/null

    # Copy everything to a temporary directory so that necessary changes can be
    # made without modifying the original files.
    tmpdir=$(mktemp -d)
    trap "rm -fr ${tmpdir@Q}" EXIT
    rsync --filter=':- .gitignore' -rtL "$pkg_dir/" "$tmpdir"

    # Use the absolute path to git directory.
    echo "gitdir: $gitdir" > "$tmpdir/.git"
    # Append anything else that may be in the file.
    sed '/^gitdir:/d' "$pkg_dir/.git" >> "$tmpdir/.git"

    "${pip_install[@]}" "$tmpdir"
    rm -fr "$tmpdir"
  fi
done
