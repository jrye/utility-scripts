#!/usr/bin/env bash
set -euo pipefail

pylint_cmd=pylint
# Check if running in a virtual environment.
if [[ -n ${VIRTUAL_ENV:-} ]]
then
  # Use the environment's pylint executable. Linting will not work with the
  # system's executable when running in a virtual environment.
  pylint_cmd=${VIRTUAL_ENV}/bin/pylint
  if [[ ! -e $pylint_cmd ]]
  then
    pip install --upgrade pylint
  fi
else
  echo "warning: running outside of virtual environment" >&2
fi

echo "Linting:"
for arg in "$@"
do
  echo "- $arg"
done
"$pylint_cmd" "$@"
