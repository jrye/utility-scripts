#!/usr/bin/env bash
set -euo pipefail

SELF=$(readlink -f "${BASH_SOURCE[0]}")
EXE_DIR=${SELF%/*}

DEFAULT_VENV_DIR=$(readlink -f venv)

function show_help()
{
  cat <<HELP
SYNOPSIS

  Install packages in a virtual environment. The virtual environment will be
  created if necessary.

USAGE

  ${0##*/} [-h] [-u] [-v <venv path>] [<package path> [<package path> ...]]

OPTIONS

  -h
    Show this message and exit.

  -u
    Upgrade pip in the virtual environment.

  -v <venv path>
    Create the virtual environment at the given path. If not given, the
    following default path will be used:

      $DEFAULT_VENV_DIR

  <package path>
    A path to an installable Python package.

HELP
  exit "$1"
}

upgrade=false
venv_dir=$DEFAULT_VENV_DIR
while getopts 'huv:' flag
do
  case "$flag" in
    u)
      upgrade=true
      ;;
    v)
      venv_dir=$OPTARG
      ;;
    h)
      show_help 0
      ;;
    *)
      show_help 1
      ;;
  esac
done
shift $((OPTIND - 1))

if [[ -z ${VIRTUAL_ENV:+x} ]]
then
  # Create the virtual environment as necessary.
  if [[ ! -e $venv_dir/bin/activate ]]
  then
    python3 -m venv "$venv_dir"
  fi

  # Activate it.
  source "$venv_dir/bin/activate"

elif [[ ! -e $VIRTUAL_ENV ]]
then
  echo "ERROR: Current in virtual environment $VIRTUAL_ENV but it no longer exists." >&2
  exit 1

elif [[ $VIRTUAL_ENV != $venv_dir ]]
then
  echo "WARNING: Using currently active virtual environment: $VIRTUAL_ENV" >&2
fi

# Upgrade pip if requested.
if $upgrade
then
  pip3 install --upgrade pip
fi

"$EXE_DIR/pip-install.sh" "$@"
