# Prepend items to a list in an environment variable. Items already in list will
# not be added again.
function prepend_to_list()
{
  declare -n list_var=$1
  local delimiter=$2
  shift 2
  declare -a args
  args=("$@")
  for ((i=${#args[@]}-1; i>= 0; i--))
  do
    if [[ -z ${list_var:+x} ]]
    then
      list_var=${args[$i]}
    elif [[ "${delimiter}${list_var}${delimiter}" != *"${delimiter}${args[$i]}${delimiter}"* ]]
    then
      list_var=${args[$i]}${delimiter}$list_var
    fi
  done
  export list_var
}

# Wrapper around prepend_to_list for the PATH environment variable.
function prepend_to_PATH()
{
  prepend_to_list PATH ':' "$@"
  export PATH
}

# Wrapper around prepend_to_list for the PYTHONPATH environment variable.
function prepend_to_PYTHONPATH()
{
  prepend_to_list PYTHONPATH ':' "$@"
  export PYTHONPATH
}
