---
title: README
author: Jan-Michael Rye
---

# Synopsis

A collection of various utility scripts to regroup common functionality for other projects. This should typically be added to other projects as a submodule.



# Bash Sources

## prepend_to_paths.sh

[prepend_to_paths.sh](sh/prepend_to_paths.sh) declares function for prepending paths to environment variables containing lists such as `PATH` and `PYTHONPATH`.

### Usage

~~~sh
source prepend_to_PYTHONPATH.sh
prepend_to_PYTHONPATH <dir path> [<dir path> ...]
~~~



# Scripts

## git-clean.sh

[git-clean.sh](scripts/git-clean.sh) removes all ignored files from the current top-level Git repository. Without confirmation it will only print a list of files that would be removed and then exit.

### Usage

See `git-clean.sh -h` for details.



## git-rsync.sh

[git-rsync.sh](scripts/git-rsync.sh) recursively copies all files in the current top-level Git repository and all of its submodules with Rsync. Files ignored by Git are also ignored by `git-rsync.sh`. It can be used to deploy a repository along with any changes in the working directory to a remote server, for example.

### Usage

`git-rsync.sh [rsync options] <destination>`

### Dependencies

* [rsync](https://rsync.samba.org/)



## pip-install.sh

[pip-install.sh](scripts/pip-install.sh) is a convenience wrapper around `pip` that handles a bug in [hatch-vcs](https://github.com/ofek/hatch-vcs)/[setuptools_scm](https://github.com/pypa/setuptools_scm) encountered when installing packages from Git submodules. The bug arises due to the use of relative paths in the `.git` file of submodules.

### Usage

See `pip-install.sh -h` for details.

### Dependencies

* [bash](https://www.gnu.org/software/bash/bash.html)
* [coreutils](https://www.gnu.org/software/coreutils/)
* [rsync](https://rsync.samba.org/)
* [sed](https://www.gnu.org/software/sed/)



## pip-install.sh

[pip-install_in_venv.sh](scripts/pip-install_in_venv.sh) is a convenience wrapper around `pip-install.sh` that will ensure that the targets are installed in a Python virtual environment.

### Usage

See `pip-install_in_venv.sh` for details.

### Dependencies

The same as `pip-install.sh`.



## pylint.sh

[pylint.sh](scripts/pylint.sh) is a convenience wrapper around [Pylint](https://pylint.pycqa.org/en/latest/) that addresses a limitation in Pylint's support for virtual environments. The limitation requires that the `pylint` executable be installed in the virtual environment rather than simply run inside of it. This script will automatically detect if a virtual environment is active, install `pylint` if it is missing and then use the executable in the virtual environment to run the command with the given arguments.

### Usage

~~~sh
pylint.sh [pylint args]
~~~

### Dependencies

The [Pylint package](https://pypi.org/project/pylint/) will be automatically installed when run inside of a Python virtual environment. Outside of a virtual environment the `pylint` executable must be available on the system path.
